%global __provides_exclude_from ^(%{python3_sitearch})/.*\\.so$
%define debug_package %{nil}
Name:           python-sqlalchemy
Version:        2.0.36
Release:        1
Summary:        SQL toolkit and object relational mapper for Python
License:        MIT
URL:            http://www.sqlalchemy.org/
Source0:        https://files.pythonhosted.org/packages/source/S/SQLAlchemy/sqlalchemy-%{version}.tar.gz
BuildRequires:  python3-devel python3-setuptools python3-pytest gcc python3-greenlet
BuildRequires:  python3-pip python3-wheel python3-pdm-pep517 python3-typing-extensions python3-pytest-xdist
BuildRequires:  python3-mypy
Requires:       python3-greenlet

%description
SQLAlchemy is an Object Relational Mapper (ORM) that provides a flexible,
high-level interface to SQL databases. It contains a powerful mapping layer
that users can choose to work as automatically or as manually, determining
relationships based on foreign keys or to bridge the gap between database
and domain by letting you define the join conditions explicitly.

%package help
Summary:       Help documents for SQLAlchemy
BuildArch:     noarch
Provides:      %{name}-doc = %{version}-%{release}
Obsoletes:     %{name}-doc < %{version}-%{release}

%description help
Help documents for SQLAlchemy.

%package -n python3-sqlalchemy
Summary:        SQL toolkit and object relational mapper for Python
%{?python_provide:%python_provide python%{python3_pkgversion}-sqlalchemy}

%description -n python3-sqlalchemy
SQLAlchemy is an Object Relational Mapper (ORM) that provides a flexible,
high-level interface to SQL databases. It contains a powerful mapping layer
that users can choose to work as automatically or as manually, determining
relationships based on foreign keys or to bridge the gap between database
and domain by letting you define the join conditions explicitly.

The python3-sqlalchemy package contains the python 3 version of the module.

%prep
%autosetup -n sqlalchemy-%{version} -p1
rm -rf doc/build # Remove unnecessary scripts for building documentation
sed -i 's/\r$//' examples/dynamic_dict/dynamic_dict.py

find . -type f -name ".gitignore" -exec rm {} \;

%build
%pyproject_build

%install
%pyproject_install


%check
PYTHONPATH=. %{__python3} -m pytest -n2 -q

%files -n python3-sqlalchemy
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%files help
%doc doc examples

%changelog
* Thu Oct 31 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 2.0.36-1
- Update to 2.0.36
  Added new parameter mapped_column.hash to ORM constructs
  Fixed bug in ORM bulk update/delete
  Fixed regression caused by fixes to joined eager loading

* Tue Oct 15 2024 xu_ping <707078654@qq.com> - 2.0.32-1
- Update to 2.0.32

* Fri Jun 16 2023 yaoxin <yao_xin001@hoperun.com> - 1.4.48-1
- Update to 1.4.48

* Mon Jun 20 2022 baizhonggui <baizhonggui@h-partners.com> - 1.4.31-2
- Fix None type judgment

* Wed Jun 01 2022 huangtianhua <huangtianhua@huawei.com> - 1.4.31-1
- update to 1.4.31

* Wed Dec 22 2021 guozhaorui <guozhaorui1@huawei.com> - 1.3.24-1
- update to 1.3.24

* Tue Aug 10 2021 OpenStack_SIG <openstack@openeuler.org> - 1.3.23-1
- update to 1.3.23

* Tue Jun 22 2021 shixuantong <shixuantong@huawei.com> - 1.3.22-3
- Type: bugfix
- ID: NA
- SUG: NA
- DESC:add gcc to BuildRequires

* Tue Mar 16 2021 tianwei <tianwei12@huawei.com> - 1.3.22-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix make check failed

* Mon Feb 1 2021 yuanxin <yuanxin24@huawei.com> - 1.3.22-1
- Upgrade version to 1.3.22

* Tue Jan 12 2021 baizhonggui <baizhonggui@huawei.com> - 1.2.11-5
- Fix building for pytest

* Mon Aug 10 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.2.11-4
- Remove python2

* Wed Jul 29 2020 lingsheng <lingsheng@huawei.com> - 1.2.11-3
- Skip test on sqlite 3.30+

* Tue Nov 26 2019 yanzhihua <yanzhihua4@huawei.com> - 1.2.11-2
- Package init

